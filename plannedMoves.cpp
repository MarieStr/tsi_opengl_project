#include <iostream>
#include <string>

// GLEW
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>

// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Shader loading class
#include "Shader.h"
// Camera loading class
#include "CameraRC.h"
// Model loading class
#include "Model.h"

// Other Libs
#include <SOIL.h>

// ------------ GLOBAL PARAMETERS -------------------------
// Dimension of the window
const GLuint WIDTH = 800, HEIGHT = 600;

// Variables of the light
GLint TIME_CYCLE(60); // In seconds, here 1 minute of day, 1 minute of night

// ------------ END OF GLOBAL PARAMETERS ------------------ 

// Main function, it is here that we initialize what is relative to the context
// of OpenGL and that we launch the main loop
int main()
{
    std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
    // ----------- Initialization of GLFW -----------------
    glfwInit();

    // Specify the OpenGL version to be used (3.3)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //.3
    // Disable the deprecated functions
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    // Necesary for Mac OS 
    #ifdef __APPLE__
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif
    // Prevent the change of dimension of the window
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    // ----------------------------------------------------
    
    // Create the application window
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Planned moves", nullptr, nullptr);    
    if (window == nullptr)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    
    // Associate the created window with the OpenGL context
    glfwMakeContextCurrent(window);
    // Associate the callback with the pressure of a key on the keyboard
    glfwSetKeyCallback(window, key_callback);

    // ----------- Initialization of GLEW -----------------

    // Variable global that allows to ask the GLEW library to find the modern 
    // functions of OpenGL
    glewExperimental = GL_TRUE;
    // Initializing GLEW to retrieve pointers from OpenGL functions
    if (glewInit() != GLEW_OK)
    {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return -1;
    }    

    // Transform the window dimensions to the dimensions of OpenGL (between -1 and 1)
    int width, height;
    // Recover the dimensions of the window created above
    glfwGetFramebufferSize(window, &width, &height); 
    glViewport(0, 0, width, height);

    // ----------------------------------------------------

    // Allow to test the depth information
    glEnable(GL_DEPTH_TEST);



    
    // Build and compile our vertex and fragment shaders
    Shader shaders("shaders/plannedMoves/default.vertexshader", 
        "shaders/plannedMoves/default.fragmentshader");




    
    // Declare the positions and uv coordinates in the same buffer
    GLfloat vertices[] = {
        /*     Positions    |      Normales     |     UV     */
        -20.0f,  0.0f, -20.0f,   0.0f, 1.0f, 0.0f,   1.0f, 1.0f, // Top Left
        -20.0f,  0.0f,  20.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // Bottom Left
         20.0f,  0.0f, -20.0f,   0.0f, 1.0f, 0.0f,   0.0f, 1.0f, // Top Right
         20.0f,  0.0f,  20.0f,   0.0f, 1.0f, 0.0f,   0.0f, 0.0f,  // Bottom Right
    };

    // The table of indices to rebuild our triangles
    GLushort indices[] = {  
        0, 1, 2,
        1, 3, 2,
    }; 



    
    // Declare the identifiers of our VAO, VBO and EBO
    GLuint VAO, VBO, EBO;
    // Inform OpenGL to generate one VAO
    glGenVertexArrays(1, &VAO);
    // Inform OpenGL to generate one VBO
    glGenBuffers(1, &VBO);
    // Inform OpenGL to generate one EBO
    glGenBuffers(1, &EBO); 
    
    // Bind the VAO
    glBindVertexArray(VAO);
    // Bind and fill the VBO
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // Bind and fill the EBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    // Color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), 
        (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);
    // UV coordinates
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), 
        (GLvoid*)(6 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);
    
    // VBO is detached from the current buffer in the OpenGL context
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // VAO is detached from the current object in the OpenGL context
    glBindVertexArray(0);
    // EBO is detached (the last one!)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); 



    // Declare the texture identifier
    GLuint texture; 
    // Generate the texture
    glGenTextures(1, &texture); 
    // Bind the texture created in the global context of OpenGL
    glBindTexture(GL_TEXTURE_2D, texture); 
    // Method of wrapping
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); 
    // Filtering method
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    
    // Loading the image file using the SOIL lib
    int twidth, theight;
    unsigned char* data = SOIL_load_image("textures/grass3.png", 
        &twidth, &theight, 0, SOIL_LOAD_RGB);
    // Associate the image data with texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, twidth, theight, 
        0, GL_RGB, GL_UNSIGNED_BYTE, data);
    // Generate the mipmap
    glGenerateMipmap(GL_TEXTURE_2D);
    // Free the memory
    SOIL_free_image_data(data);
    // Unbind the texture
    glBindTexture(GL_TEXTURE_2D, 0);




    // Camera
    Camera camera(glm::vec3(0.0f, 0.8f, 20.0f), window);

    
    // Load models
    Model oCar((GLchar*)"./model/objects/Car/car.obj");
    Model oRoad((GLchar*)"./model/objects/road2/road2.obj");


    // Definition of the angle of the sun
    glm::mat4 rot;
    GLfloat angle = (GLfloat) M_PI/2;

    // Car variables
    // Initialisation of times and timelaps for the movement
    GLfloat deltaT(0);
    GLfloat lastT;
    lastT = glfwGetTime();

    // Initialisation of car front, speed and position
    glm::vec3 carFront = glm::vec3(0.0f, 0.0f, 1.0f);
    GLfloat car_speed(6.0f);
    glm::vec3 carPos = glm::vec3(4.0f, 0.5f, 4.0f);


    
    // The main loop of the program
    while (!glfwWindowShouldClose(window))
    {
        // Retrieve events and call the corresponding callback functions
        glfwPollEvents();
        camera.Do_Movement();
        camera.Switch_Mode();

        // Get the view position for the specular light
        glUniform3f(glGetUniformLocation(shaders.Program, "viewPos"), camera.Position.x, camera.Position.y, camera.Position.z);


        // Replace the background color buffer of the window
        // ----- Ligne to delete after
        // glClearColor(0.2f, 0.2f, 0.8f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        
        // Inform OpenGL that we want to use the shaders we have created
        shaders.Use();
        
        
        // ---------- Fill the global variables of the shaders ------------
        // On récupère les identifiants des variables globales du shader
        GLint modelLoc = glGetUniformLocation(shaders.Program, "model");
        // Projection matrix (generate a perspective projection matrix)
        glm::mat4 projection;
        projection = glm::perspective(45.0f, (GLfloat)WIDTH / (GLfloat)HEIGHT, 0.1f, 350.0f);
        // Camera matrix
        glm::mat4 view;
        view = camera.GetViewMatrix();

        float radius = 30.0f;
        float camX   = sin(glfwGetTime()/6) * radius;
        float camZ   = cos(glfwGetTime()/6) * radius;
        view = glm::lookAt(glm::vec3(camX, 10.0f, camZ), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        glUniformMatrix4fv(glGetUniformLocation(shaders.Program,"view"), 1, GL_FALSE, glm::value_ptr(view));
        

        glm::mat4 model;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));	
        glUniformMatrix4fv(glGetUniformLocation(shaders.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));


        // Activate texture 0
        glActiveTexture(GL_TEXTURE0);
        // Bind the texture
        glBindTexture(GL_TEXTURE_2D, texture);
        // Associate the texture with the shader
        glUniform1i(glGetUniformLocation(shaders.Program, "modelTexture"), 0);
        // Bind the VAO as a current object in the context of OpenGL
        glBindVertexArray(VAO);


        // Position and color of the sun light
        GLint lightPos = glGetUniformLocation(shaders.Program, "lightPos");
        GLint lightColor = glGetUniformLocation(shaders.Program, "lightColor");
        GLint ambientStrength = glGetUniformLocation(shaders.Program, "ambientStrength");
        GLint specularStrength = glGetUniformLocation(shaders.Program, "specularStrength");
        GLfloat ambStr(0.7f);
        GLfloat specStr(0.3f);

        // The sun light is placed relatively to the camera, so it follows the "user"
        glm::vec4 lPos(camera.Position.x, camera.Position.y + 300.0f, camera.Position.z, 1.0f);
        glm::vec3 lColor(1.0f, 1.0f, 1.0f);

        
        // Rotation matrice of the sun aroung the Z axe
        rot = glm::rotate(rot, angle, glm::vec3(0.0f, 0.0f, 1.0f));
        lPos = rot * lPos;

        // Color of the sky
        glm::vec3 skyColor = glm::vec3(0.0f, 0.5f, 1.0f);

        // Coloring the sky
        glClearColor(skyColor.x, skyColor.y, skyColor.z, 1.0f);
        // Update of the global variables for light
        glUniform3f(lightPos, lPos.x, lPos.y, lPos.z);
        glUniform3f(lightColor, lColor.x, lColor.y , lColor.z);
        glUniform1f(ambientStrength, ambStr);
        glUniform1f(specularStrength, specStr);

        
        // Recover the identifiers of the global variables of the shader
        GLint projLoc  = glGetUniformLocation(shaders.Program, "projection");
        // Update the global variables of the shader
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        // Recover the identifiers of the global variables of the shader
        GLint viewLoc  = glGetUniformLocation(shaders.Program, "view");
        // Update the global variables of the shader
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

        // Recover the identifiers of the global variables of the shader
        modelLoc = glGetUniformLocation(shaders.Program, "model");
        // Update the global variables of the shaders
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));


        
        // ------------ DRAWING -------------------

        // --- Draw the ground:
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);


        // --- Draw the objects:

        // Moving Car -----------------------
        // Update of time variables
        deltaT = glfwGetTime() - lastT;
        lastT = glfwGetTime();

        // Intialisation of the model
        model = glm::mat4(1.0f);
        model = glm::rotate(model,
                (carFront.z - glm::length(carFront.z)
                    + carFront.x) * (GLfloat)M_PI/2,
                glm::vec3(0.0f, 1.0f, 0.0f));

        // Choose a direction randomly
        // 0 -> straight; 1 -> right; 2 -> left
        GLint dir;
        dir = rand()%3;

        // If the car reaches the limit of the square
        // He turns right
        if(glm::length(carPos.x) + 9.2f >= (18.4f + deltaT*car_speed)
                    || carPos.z + 9.2f >= (18.4f + deltaT*car_speed)
                    || carPos.z - 9.2f <= -(18.4f + deltaT*car_speed))
        {
            dir = 1 ;
        }

        // If the car is moving according the x and he comes to the board of the square
        if (carFront.x != 0 && glm::mod(carPos.x + 3.8f, 9.2f) <= deltaT*car_speed) 
        {   
            // Turns right
            if (dir == 1) 
            {
                carFront = glm::vec3(0.0f, 0.0f, carFront.x);
                carPos.x += carFront.x * deltaT * car_speed;
                carPos.z += carFront.z * deltaT * car_speed;
            }
            // Turns left
            else if (dir == 2) 
            {
                carFront = glm::vec3(0.0f, 0.0f, -carFront.x);
                carPos.x += carFront.x * deltaT * car_speed;
                carPos.z += carFront.z * deltaT * car_speed;
            }
            // Goes straight
            else {
                carPos.x += carFront.x * deltaT * car_speed;
                carPos.z += carFront.z * deltaT * car_speed;
            }
            
        } 
        // If the car is moving according the z and he comes to the board of the square
        // He turns right
        else if (carFront.z != 0 && glm::mod(carPos.z + 3.8f, 9.2f) <= deltaT*car_speed) {
            // Turns right
            if (dir == 1) 
            {
                carFront = glm::vec3(-carFront.z, 0.0f, 0.0f);
                carPos.x += carFront.x * deltaT * car_speed;
                carPos.z += carFront.z * deltaT * car_speed;
            }
            // Turns left
            else if (dir == 2) 
            {
                carFront = glm::vec3(carFront.z, 0.0f, 0.0f);
                carPos.x += carFront.x * deltaT * car_speed;
                carPos.z += carFront.z * deltaT * car_speed;
            }
            // Goes straight
            else {
                carPos.x += carFront.x * deltaT * car_speed;
                carPos.z += carFront.z * deltaT * car_speed;
            }
            
        } 
        // Else he goes straight
        else {
            carPos.x += carFront.x * deltaT * car_speed;
            carPos.z += carFront.z * deltaT * car_speed;
        }
        
        // Draw the car
        glm::mat4 movingCar;
        movingCar = glm::translate(movingCar,
                        glm::vec3(carPos.x - carFront.z * 0.3f, carPos.y, carPos.z + carFront.x * 0.3f));
        model = movingCar * model;
        model = glm::scale(model, glm::vec3(0.004f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        oCar.Draw(shaders);



        // ----------- DRAW BLOCKS ----------------
        glm::vec3 blockPosition;
        for (int i=0; i<4; i++) 
        {
            if (i==0)
            {   // Bottom right
                blockPosition = glm::vec3(1.0f, 1.0f, 1.0f);
            }
            else if (i==1)
            {   // Up right
                blockPosition = glm::vec3(1.0f, 1.0f, -1.0f);
            }
            else if (i==2)
            {   // Up left
                blockPosition = glm::vec3(-1.0f, 1.0f, -1.0f);
            }
            else
            {   // Bottom left
                blockPosition = glm::vec3(-1.0f, 1.0f, 1.0f);
            }

            // ----------- DRAW THE ROAD ----------------
            // Up left part
            model = glm::mat4(1.0f);
            model = glm::translate(model, glm::vec3(4.6f * blockPosition.x, 0.2f, 4.6f * blockPosition.z));
            model = glm::scale(model, glm::vec3(0.005f));
            model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3( -1.0f, 0.0f, 0.0f));
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
            oRoad.Draw(shaders);

            // Bottom left part
            model = glm::mat4(1.0f);
            model = glm::translate(model, glm::vec3(13.6f * blockPosition.x, 0.2f, 4.6f * blockPosition.z));
            model = glm::scale(model, glm::vec3(0.005f));
            model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3( -1.0f, 0.0f, 0.0f));
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
            oRoad.Draw(shaders);

            // Bottom right part
            model = glm::mat4(1.0f);
            model = glm::translate(model, glm::vec3(13.6f * blockPosition.x, 0.2f, 13.6f * blockPosition.z));
            model = glm::scale(model, glm::vec3(0.005f));
            model = glm::rotate(model, (GLfloat) -M_PI/2, glm::vec3(0.0f, 1.0f, 0.0f));
            model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3( -1.0f, 0.0f, 0.0f));
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
            oRoad.Draw(shaders);
            
            // Up left part
            model = glm::mat4(1.0f);
            model = glm::translate(model, glm::vec3(4.6f * blockPosition.x, 0.2f, 13.6f * blockPosition.z));
            model = glm::scale(model, glm::vec3(0.005f));
            model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(0.0f, 1.0f, 0.0f));
            model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3( -1.0f, 0.0f, 0.0f));
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
            oRoad.Draw(shaders);
        }


        // Reinit
        // Model matrix (translation, rotation and scale)
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
        //model = glm::scale(model, glm::vec3(0.2f));
        // Update the global variables of the shader
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));



        // VBA is detached from the current object in the OpenGL context
        glBindVertexArray(0);

        // Exchange rendering buffers to update the window with new content
        glfwSwapBuffers(window);
        
    }
    

    // Delete the objects and buffer that we created earlier
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
    
    // End of the program, we clean up the context created by the GLFW
    glfwTerminate();
    return 0;
}
