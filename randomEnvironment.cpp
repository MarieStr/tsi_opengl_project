#include <iostream>
#include <string>

// GLEW
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>

// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Shader loading class
#include "Shader.h"
// Camera loading class
#include "CameraRC.h"
// Model loading class
#include "Model.h"

// Other Libs
#include <SOIL.h>

// ------------ GLOBAL PARAMETERS -------------------------
// Dimension of the window
const GLuint WIDTH = 800, HEIGHT = 600;

// Number of blocks we want
GLint nbBlock(4) ;
// Maximal number of blocks aligned along the x axe
// If the number of blocks is bigger, a new line of blocks is created along x axe
GLint nbBlocXMax(3);


// ------------ END OF GLOBAL PARAMETERS ------------------ 

// Main function, it is here that we initialize what is relative to the context
// of OpenGL and that we launch the main loop
int main()
{
    std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
    // ----------- Initialization of GLFW -----------------
    glfwInit();

    // Specify the OpenGL version to be used (3.3)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //.3
    // Disable the deprecated functions
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    // Necesary for Mac OS 
    #ifdef __APPLE__
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif
    // Prevent the change of dimension of the window
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    // ----------------------------------------------------
    
    // Create the application window
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Random Environment", nullptr, nullptr);    
    if (window == nullptr)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    
    // Associate the created window with the OpenGL context
    glfwMakeContextCurrent(window);
    // Associate the callback with the pressure of a key on the keyboard
    glfwSetKeyCallback(window, key_callback);

    // ----------- Initialization of GLEW -----------------

    // Variable global that allows to ask the GLEW library to find the modern 
    // functions of OpenGL
    glewExperimental = GL_TRUE;
    // Initializing GLEW to retrieve pointers from OpenGL functions
    if (glewInit() != GLEW_OK)
    {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return -1;
    }    

    // Transform the window dimensions to the dimensions of OpenGL (between -1 and 1)
    int width, height;
    // Recover the dimensions of the window created above
    glfwGetFramebufferSize(window, &width, &height); 
    glViewport(0, 0, width, height);

    // ----------------------------------------------------

    // Allow to test the depth information
    glEnable(GL_DEPTH_TEST);



    
    // Build and compile our vertex and fragment shaders
    Shader shaders("shaders/randomEnvironment/default.vertexshader", 
        "shaders/randomEnvironment/default.fragmentshader");




    
    // Declare the positions and uv coordinates in the same buffer
    GLfloat vertices[] = {
        /*     Positions    |      Normales     |     UV     */
        - 20.0f,  0.0f, - 20.0f,   0.0f, 1.0f, 0.0f,   1.0f, 1.0f, // Top Left
        - 20.0f,  0.0f,  300.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // Bottom Left
         300.0f,  0.0f, - 20.0f,   0.0f, 1.0f, 0.0f,   0.0f, 1.0f, // Top Right
         300.0f,  0.0f,  300.0f,   0.0f, 1.0f, 0.0f,   0.0f, 0.0f,  // Bottom Right
    };

    // The table of indices to rebuild our triangles
    GLushort indices[] = {  
        0, 1, 2,
        1, 3, 2,
    }; 



    
    // Declare the identifiers of our VAO, VBO and EBO
    GLuint VAO, VBO, EBO;
    // Inform OpenGL to generate one VAO
    glGenVertexArrays(1, &VAO);
    // Inform OpenGL to generate one VBO
    glGenBuffers(1, &VBO);
    // Inform OpenGL to generate one EBO
    glGenBuffers(1, &EBO); 
    
    // Bind the VAO
    glBindVertexArray(VAO);
    // Bind and fill the VBO
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // Bind and fill the EBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    // Color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), 
        (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);
    // UV coordinates
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), 
        (GLvoid*)(6 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);
    
    // VBO is detached from the current buffer in the OpenGL context
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // VAO is detached from the current object in the OpenGL context
    glBindVertexArray(0);
    // EBO is detached (the last one!)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); 



    // Declare the texture identifier
    GLuint texture; 
    // Generate the texture
    glGenTextures(1, &texture); 
    // Bind the texture created in the global context of OpenGL
    glBindTexture(GL_TEXTURE_2D, texture); 
    // Method of wrapping
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); 
    // Filtering method
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    
    // Loading the image file using the SOIL lib
    int twidth, theight;
    unsigned char* data = SOIL_load_image("textures/grass3.png", 
        &twidth, &theight, 0, SOIL_LOAD_RGB);
    // Associate the image data with texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, twidth, theight, 
        0, GL_RGB, GL_UNSIGNED_BYTE, data);
    // Generate the mipmap
    glGenerateMipmap(GL_TEXTURE_2D);
    // Free the memory
    SOIL_free_image_data(data);
    // Unbind the texture
    glBindTexture(GL_TEXTURE_2D, 0);




    // Camera
    Camera camera(glm::vec3(10.0f, 5.0f, 20.0f), window);

    
    // Load models
    Model oTree((GLchar*)"./model/objects/Tree/Tree.obj");
    Model oHouse((GLchar*)"./model/objects/House/house.obj");
    Model oRoad((GLchar*)"./model/objects/road2/road2.obj");
    Model oSLampOff((GLchar*)"./model/objects/StreetLamp/Lamp.obj");
    Model oSwing((GLchar*)"./model/objects/swing/swing.obj");


    // Variables for the light
    glm::mat4 rot;
    GLfloat angle = (GLfloat) M_PI/2;


    // Choose randomly a type for each blocks
    GLint blockType[nbBlock];
    std::srand(time(0));
    for (int i =0; i < nbBlock; i++) {
        blockType[i] = std::rand()%5;
    }

    
    // The main loop of the program
    while (!glfwWindowShouldClose(window))
    {
        // Retrieve events and call the corresponding callback functions
        glfwPollEvents();
        camera.Do_Movement();
        camera.Switch_Mode();

        // Get the view position for the specular light
        glUniform3f(glGetUniformLocation(shaders.Program, "viewPos"), camera.Position.x, camera.Position.y, camera.Position.z);


        // Replace the background color buffer of the window
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        
        // Inform OpenGL that we want to use the shaders we have created
        shaders.Use();
        
        
        // ---------- Fill the global variables of the shaders ------------
        // On récupère les identifiants des variables globales du shader
        GLint modelLoc = glGetUniformLocation(shaders.Program, "model");
        // Projection matrix (generate a perspective projection matrix)
        glm::mat4 projection;
        projection = glm::perspective(45.0f, (GLfloat)WIDTH / (GLfloat)HEIGHT, 0.1f, 350.0f);
        // Camera matrix
        glm::mat4 view;
        view = camera.GetViewMatrix();

        glm::mat4 model;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));	
        glUniformMatrix4fv(glGetUniformLocation(shaders.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));


        // Activate texture 0
        glActiveTexture(GL_TEXTURE0);
        // Bind the texture
        glBindTexture(GL_TEXTURE_2D, texture);
        // Associate the texture with the shader
        glUniform1i(glGetUniformLocation(shaders.Program, "modelTexture"), 0);
        // Bind the VAO as a current object in the context of OpenGL
        glBindVertexArray(VAO);


        // Position and color of the sun light
        GLint lightPos = glGetUniformLocation(shaders.Program, "lightPos");
        GLint lightColor = glGetUniformLocation(shaders.Program, "lightColor");
        GLint ambientStrength = glGetUniformLocation(shaders.Program, "ambientStrength");
        GLint specularStrength = glGetUniformLocation(shaders.Program, "specularStrength");
        GLfloat ambStr(0.7f);
        GLfloat specStr(0.3f);

        // The sun light is placed relatively to the camera, so it follows the "user"
        glm::vec4 lPos(camera.Position.x, camera.Position.y + 300.0f, camera.Position.z, 1.0f);
        glm::vec3 lColor(1.0f, 1.0f, 1.0f);

        
        // Rotation matrice of the sun aroung the Z axe
        rot = glm::rotate(rot, angle, glm::vec3(0.0f, 0.0f, 1.0f));
        lPos = rot * lPos;

        // Color of the sky
        glm::vec3 skyColor = glm::vec3(0.0f, 0.5f, 1.0f);

        // Coloring the sky
        glClearColor(skyColor.x, skyColor.y, skyColor.z, 1.0f);
        // Update of the global variables for light
        glUniform3f(lightPos, lPos.x, lPos.y, lPos.z);
        glUniform3f(lightColor, lColor.x, lColor.y , lColor.z);
        glUniform1f(ambientStrength, ambStr);
        glUniform1f(specularStrength, specStr);

        
        // Recover the identifiers of the global variables of the shader
        GLint projLoc  = glGetUniformLocation(shaders.Program, "projection");
        // Update the global variables of the shader
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        // Recover the identifiers of the global variables of the shader
        GLint viewLoc  = glGetUniformLocation(shaders.Program, "view");
        // Update the global variables of the shader
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

        // Recover the identifiers of the global variables of the shader
        modelLoc = glGetUniformLocation(shaders.Program, "model");
        // Update the global variables of the shaders
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));


        
        // ------------ DRAWING -------------------

        // ----------- DRAW THE GROUND ----------------
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);


        // ----------- DRAW BLOCKS ----------------
        // The first block is located at the origin (its up left corner)
        glm::vec3 blockCenter = glm::vec3(0.0f, 0.0f, 0.0f);
        GLint nbDrawnBlock(0);

        // We first draw blocks along the x axe 
        // Until the maximum number of block for the x axe is reached
        // Then a new x line is started
        while (nbDrawnBlock < nbBlock)
        {   
            blockCenter.x = 0.0f;

            // For each block we draw its road and set of object according to its type
            for (int nbBlockX=0; nbBlockX < nbBlocXMax; nbBlockX++) 
            {   
                // If we have already drawn enough blocks
                // We break the for loop
                if (nbDrawnBlock == nbBlock)
                    nbBlockX = nbBlocXMax;
                else
                {

                
                    // ----------- DRAW THE ROAD ----------------
                    // Up left part
                    model = glm::mat4(1.0f);
                    model = glm::translate(model, glm::vec3(blockCenter.x + 4.6f, 0.2f, blockCenter.z + 4.6f));
                    model = glm::scale(model, glm::vec3(0.005f));
                    model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3( -1.0f, 0.0f, 0.0f));
                    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                    oRoad.Draw(shaders);

                    // Bottom left part
                    model = glm::mat4(1.0f);
                    model = glm::translate(model, glm::vec3(blockCenter.x + 13.6f, 0.2f, blockCenter.z + 4.6f));
                    model = glm::scale(model, glm::vec3(0.005f));
                    model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3( -1.0f, 0.0f, 0.0f));
                    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                    oRoad.Draw(shaders);

                    // Bottom right part
                    model = glm::mat4(1.0f);
                    model = glm::translate(model, glm::vec3(blockCenter.x + 13.6f, 0.2f, blockCenter.z + 13.6f));
                    model = glm::scale(model, glm::vec3(0.005f));
                    model = glm::rotate(model, (GLfloat) -M_PI/2, glm::vec3(0.0f, 1.0f, 0.0f));
                    model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3( -1.0f, 0.0f, 0.0f));
                    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                    oRoad.Draw(shaders);

                    // Up right part
                    model = glm::mat4(1.0f);
                    model = glm::translate(model, glm::vec3(blockCenter.x + 4.6f, 0.2f, blockCenter.z + 13.6f));
                    model = glm::scale(model, glm::vec3(0.005f));
                    model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(0.0f, 1.0f, 0.0f));
                    model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3( -1.0f, 0.0f, 0.0f));
                    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                    oRoad.Draw(shaders);


                    // ---------------- DRAW SET OF OBJECTS -------------------
                    // According to its blockType

                    // ---------------- BLOCK TYPE: two houses -------------------
                    if (blockType[nbDrawnBlock] == 0) 
                    {
                        // Draw a house
                        model = glm::mat4(1.0f);
                        model = glm::translate(model, glm::vec3(blockCenter.x + 9.1f, 0.0f, blockCenter.z + 8.3f));
                        model = glm::scale(model, glm::vec3(0.04f));
                        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                        oHouse.Draw(shaders);

                        // Draw a house
                        model = glm::mat4(1.0f);
                        model = glm::translate(model, glm::vec3(blockCenter.x + 9.1f, 0.0f, blockCenter.z + 11.1f));
                        model = glm::scale(model, glm::vec3(0.04f));
                        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                        oHouse.Draw(shaders);

                    }
                    // ---------------- BLOCK TYPE: house and tree -------------------
                    else if (blockType[nbDrawnBlock] == 1) 
                    {
                        // Draw a house
                        model = glm::mat4(1.0f);
                        model = glm::translate(model, glm::vec3(blockCenter.x + 9.1f, 0.0f, blockCenter.z + 8.3f));
                        model = glm::scale(model, glm::vec3(0.04f));
                        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                        oHouse.Draw(shaders);

                        // Draw a tree
                        model = glm::mat4(1.0f);
                        model = glm::translate(model, glm::vec3(blockCenter.x + 10.6f, 0.0f, blockCenter.z + 10.6f));
                        model = glm::scale(model, glm::vec3(0.5f));
                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                        oTree.Draw(shaders);
                    }
                    // ---------------- BLOCK TYPE: house and swing -------------------
                    else if (blockType[nbDrawnBlock] == 2) 
                    {
                        // Draw a house
                        model = glm::mat4(1.0f);
                        model = glm::translate(model, glm::vec3(blockCenter.x + 9.1f, 0.0f, blockCenter.z + 11.1f));
                        model = glm::scale(model, glm::vec3(0.04f));
                        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                        oHouse.Draw(shaders);

                        // Draw a swing
                        model = glm::mat4(1.0f);
                        model = glm::translate(model, glm::vec3(blockCenter.x + 9.1f, 0.0f, blockCenter.z + 7.6f));
                        model = glm::scale(model, glm::vec3(0.005f));
                        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3( 0.0f, 0.0f, 1.0f));
                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                        oSwing.Draw(shaders);

                    }
                    // ------------------- BLOCK TYPE: four trees ------------------
                    else 
                    {
                        // Draw a tree
                        model = glm::mat4(1.0f);
                        model = glm::translate(model, glm::vec3(blockCenter.x + 7.6f, 0.0f, blockCenter.z + 7.6f));
                        model = glm::scale(model, glm::vec3(0.5f));
                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                        oTree.Draw(shaders);

                        // Draw a tree
                        model = glm::mat4(1.0f);
                        model = glm::translate(model, glm::vec3(blockCenter.x + 10.6f, 0.0f, blockCenter.z + 10.6f));
                        model = glm::scale(model, glm::vec3(0.5f));
                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                        oTree.Draw(shaders);

                        // Draw a tree
                        model = glm::mat4(1.0f);
                        model = glm::translate(model, glm::vec3(blockCenter.x + 7.6f, 0.0f, blockCenter.z + 10.6f));
                        model = glm::scale(model, glm::vec3(0.5f));
                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                        oTree.Draw(shaders);

                        // Draw a tree
                        model = glm::mat4(1.0f);
                        model = glm::translate(model, glm::vec3(blockCenter.x + 10.6f, 0.0f, blockCenter.z + 7.6f));
                        model = glm::scale(model, glm::vec3(0.5f));
                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                        oTree.Draw(shaders);
                    }

                    // We increment the x position of the block center to draw the next block
                    blockCenter.x += 18.1f;
                    nbDrawnBlock ++;
                                        
                }
            }
            
            // The maximal number of block along the x axe is reached
            // So, we increment the z position of the block center to start a new line
            // and then draw the next block
            blockCenter.z += 18.1f;
            
        }
    


        // Reinit
        // Model matrix (translation, rotation and scale)
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
        //model = glm::scale(model, glm::vec3(0.2f));
        // Update the global variables of the shader
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));



        // VBA is detached from the current object in the OpenGL context
        glBindVertexArray(0);

        // Exchange rendering buffers to update the window with new content
        glfwSwapBuffers(window);
        
    }
    

    // Delete the objects and buffer that we created earlier
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
    
    // End of the program, we clean up the context created by the GLFW
    glfwTerminate();
    return 0;
}
