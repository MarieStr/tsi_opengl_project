# Thoughts

## Ground

- must be illimited
- apply a not-shitty texture on it


## Light source

- fixed during the night -> opened when night is here
- moving with the mouse during the day
- night at the start and night at the end


## Objects

- find a fixed light (christmas lights, ovni, street light, etc.)
- find a tree (TanenBaum)

## Command

- **camera.DoMovements()** : link the camera to the mouse's movement
  - -> comment it to fix the camera
