# IT3 TSI - Project OpenGL

We had to implement four modules :

1. Dynamic light : "day to night" cycle
2. Realistic camera : "walking" view and an aerial view
3. Random world generation
4. Adding planned moves : one or more vehicles are moving according to a predefined or automatic pattern

**WARNING: All of our classes correctly run under Ubuntu but we are aware of some functionalities, especially the car movement, not being handled under Mac.**

## Getting started

### Prerequisities

* OS: Linux (Ubuntu)
* CMake

## Running

1. Move to the folder containing the main.cpp file. 
2. Open a terminal in the folder.
3. Initialise the build of the project:
    ```
    cmake ./
    ```
2. Compile the project with the following line:
    ```
    make
    ```

1. Run the project:
    ```
    ./main.cpp
    ```

To run the separate modules execute the lines :

```
# For dynamic light
./dynamicLight

# For realistic camera
./realisticCamera

# For random environment
./randomEnvironment

# For planned moves
./plannedMoves
```



## What has been implemented

A recurrent bug is in all our modules: when a project is launched, the light can go on and off in a crazy way. 
We have not been able to identify the source, but to avoid it, you just have to close and restart the project until the light bug disappears.


### Dynamic light

- [x] Model a "day to night" cycle with:
    - [x] A light source that moves in the "sky"
    - [x] A fixed light during the "night" (preferably coming from a street lamp!)
- [x] **Plus:** Play with the colors of light to generate twilight lighting


#### Parameters:
At the beginning of the `dynamicLight.cpp` file, the global variable named `TIME_CYCLE` is the duration of the day or the night. 
It is settled at 60s, for 60s of day and 60s of night.

**Be careful** not to set it under 50s, or the module will bug.


#### Moving the camera:

You can move with the keys `zqsd` and point the direction you want to go with the mouse.

* `z`: zoom  
* `q`: left  
* `s`: dezoom  
* `d`: right



### Realistic camera

- [x] Modify the Camera class to constrain the movement parallel to the ground, as
if we were "walking" on the ground
- [x] Add a visualisation mode where the camera is situated in the air and pointing permanently to the ground (i.e. aerial imaging)
- [x] **Plus:** allow changing the viewing mode by pressing a key on the keyboard


#### Moving the camera

There are four modes in this module:
* the realistic mode, which is the default one
* the aerial mode
* the free mode
* the animated mode

You can switch between modes by pressing the space bar.

* In the realistic mode: the "walking" view

    You can point the direction you want to go with the mouse.

    * `z`: forward
    * `q`: left
    * `s`: backward
    * `d`: right

* In the aerial mode : the mouse pointer is disabled

    * `z`: "north"
    * `q`: "west"
    * `s`: "south"
    * `d`: "east"


### Random Environment

- [x] Make sure that the world is generated randomly at the start of the application:
    - [x] the different objects are randomly placed in the space, but consistent (no
    levitation or collision between objects)
- [x] **Plus:** allow the parameterization of the number and type of objects that we
    want to generate

#### Parameters:
At the beginning of the `randomEnvironment.cpp` file, they are different global variables that can be modified to parameterise the program.
1. `nbBlock` determines the number of blocks you want to draw: 
    - one block is a set of roads and one parcel 
    - a parcel type is determined randomly
2. `nbBlockXMax` determines the maximal number of blocks you want to draw along the x axe. 
We draw lines of blocks along the y axe with a maximal number of columns. 
So if the number of blocks wanted is higher than the number allowed along the x axe, a new y-line is started.


#### Moving the camera
Same as in the realistic camera module.


### Planned Moves

- [x] Move one or more vehicles according to a predefined or automatic pattern
- [x] **Plus:** move the camera independently, like an animated movie

**Be aware** that the camera can't be moved manually. Only the animated mode is activated.



### Main
The `main.cpp` file is a mix of the four modules we have implemented. It has:
- The "day to night" cycle from `dynamicLight`.
- The different view modes from `realisticCamera`.
- The random determination of the environment from `randomEnvironment`. 
A difference is that the main executable draws the environment square by square (so four blocks by four blocks) rather than block by block.
- And one car which moves following a pattern from `plannedMoves`. The car can move outside the drawn square(s).

#### Parameters:
At the beginning of the `main.cpp` file, they are different global variables that can be modified to parameterise the program.

1. `TIME_CYCLE` is the duration of the day or the night. 
It is settled at 120, for 120s of day and 120s of night.

    **Be careful** not to set it under 60s, or the module will bug.
1. `nbSquare` determines the number of squares you want to draw: 
    - one square is four blocks
    - one block is a set of roads and one parcel 
    - a parcel type is determined randomly
2. `nbSquareXMax` determines the maximal number of squares you want to draw along the x axe. 
We draw lines of squares along the y axe with a maximal number of columns. 
So if the number of squares wanted is higher than the number allowed along the x axe, a new y-line is started.


#### Moving the camera
Same as in the realistic camera module.


---
Authors:

* Marie STRETTI: @MarieStr
* Fanny VIGNOLLES: @fvgls

