#include <iostream>
#include <string>

// GLEW
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>

// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Shader loading class
#include "Shader.h"
// Camera loading class
#include "Camera.h"
// Model loading class
#include "Model.h"

// Other Libs
#include <SOIL.h>

// ------------ GLOBAL PARAMETERS -------------------------
// Dimension of the window
const GLuint WIDTH = 800, HEIGHT = 600;

// Variables of the light
GLfloat TWILIGHT_ANGLE(2*M_PI/3);
GLfloat DAWN_ANGLE(4*M_PI/3);
GLint TIME_CYCLE(60); // In seconds, here 1 minute of day, 1 minute of night

// ------------ END OF GLOBAL PARAMETERS ------------------ 

// Main function, it is here that we initialize what is relative to the context
// of OpenGL and that we launch the main loop
int main()
{
    std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
    // ----------- Initialization of GLFW -----------------
    glfwInit();

    // Specify the OpenGL version to be used (3.3)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //.3
    // Disable the deprecated functions
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    // Necesary for Mac OS 
    #ifdef __APPLE__
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif
    // Prevent the change of dimension of the window
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    // ----------------------------------------------------
    
    // Create the application window
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Dynamic Light", nullptr, nullptr);    
    if (window == nullptr)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    
    // Associate the created window with the OpenGL context
    glfwMakeContextCurrent(window);
    // Associate the callback with the pressure of a key on the keyboard
    glfwSetKeyCallback(window, key_callback);

    // ----------- Initialization of GLEW -----------------

    // Variable global that allows to ask the GLEW library to find the modern 
    // functions of OpenGL
    glewExperimental = GL_TRUE;
    // Initializing GLEW to retrieve pointers from OpenGL functions
    if (glewInit() != GLEW_OK)
    {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return -1;
    }    

    // Transform the window dimensions to the dimensions of OpenGL (between -1 and 1)
    int width, height;
    // Recover the dimensions of the window created above
    glfwGetFramebufferSize(window, &width, &height); 
    glViewport(0, 0, width, height);

    // ----------------------------------------------------

    // Allow to test the depth information
    glEnable(GL_DEPTH_TEST);



    
    // Build and compile our vertex and fragment shaders
    Shader shaders("shaders/dynamicLight/default.vertexshader", 
        "shaders/dynamicLight/default.fragmentshader");




    
    // Declare the positions and uv coordinates in the same buffer
    GLfloat vertices[] = {
        /*     Positions    |      Normales     |     UV     */
        -100.0f,  0.0f, -100.0f,   0.0f, 1.0f, 0.0f,   1.0f, 1.0f, // Top Left
        -100.0f,  0.0f,  100.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // Bottom Left
         100.0f,  0.0f, -100.0f,   0.0f, 1.0f, 0.0f,   0.0f, 1.0f, // Top Right
         100.0f,  0.0f,  100.0f,   0.0f, 1.0f, 0.0f,   0.0f, 0.0f,  // Bottom Right
    };

    // The table of indices to rebuild our triangles
    GLushort indices[] = {  
        0, 1, 2,
        1, 3, 2,
    }; 



    
    // Declare the identifiers of our VAO, VBO and EBO
    GLuint VAO, VBO, EBO;
    // Inform OpenGL to generate one VAO
    glGenVertexArrays(1, &VAO);
    // Inform OpenGL to generate one VBO
    glGenBuffers(1, &VBO);
    // Inform OpenGL to generate one EBO
    glGenBuffers(1, &EBO); 
    
    // Bind the VAO
    glBindVertexArray(VAO);
    // Bind and fill the VBO
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // Bind and fill the EBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    // Color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), 
        (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);
    // UV coordinates
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), 
        (GLvoid*)(6 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);
    
    // VBO is detached from the current buffer in the OpenGL context
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // VAO is detached from the current object in the OpenGL context
    glBindVertexArray(0);
    // EBO is detached (the last one!)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); 



    // Declare the texture identifier
    GLuint texture; 
    // Generate the texture
    glGenTextures(1, &texture); 
    // Bind the texture created in the global context of OpenGL
    glBindTexture(GL_TEXTURE_2D, texture); 
    // Method of wrapping
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); 
    // Filtering method
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    
    // Loading the image file using the SOIL lib
    int twidth, theight;
    unsigned char* data = SOIL_load_image("textures/grass3.png", 
        &twidth, &theight, 0, SOIL_LOAD_RGB);
    // Associate the image data with texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, twidth, theight, 
        0, GL_RGB, GL_UNSIGNED_BYTE, data);
    // Generate the mipmap
    glGenerateMipmap(GL_TEXTURE_2D);
    // Free the memory
    SOIL_free_image_data(data);
    // Unbind the texture
    glBindTexture(GL_TEXTURE_2D, 0);




    // Camera
    Camera camera(glm::vec3(0.0f, 2.0f, 8.0f), window);

    
    // Load models
    Model oSun((GLchar*)"./model/objects/Sun/soleil.obj");
    Model oSLampOff((GLchar*)"./model/objects/StreetLamp/Lamp.obj");
    Model oSLampOn((GLchar*)"./model/objects/StreetLamp/litLamp/Lamp.obj");
    Model oHouse((GLchar*)"./model/objects/House/house.obj");
    Model oTree((GLchar*)"./model/objects/Tree/Tree.obj");
    Model oSwing((GLchar*)"./model/objects/swing/swing.obj");


    
    // The main loop of the program
    while (!glfwWindowShouldClose(window))
    {
        // Retrieve events and call the corresponding callback functions
        glfwPollEvents();
        camera.Do_Movement();

        // Get the view position for the specular light
        glUniform3f(glGetUniformLocation(shaders.Program, "viewPos"), camera.Position.x, camera.Position.y, camera.Position.z);


        // Replace the background color buffer of the window
        // ----- Ligne to delete after
        // glClearColor(0.2f, 0.2f, 0.8f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        
        // Inform OpenGL that we want to use the shaders we have created
        shaders.Use();
        
        
        // ---------- Fill the global variables of the shaders ------------
        // On récupère les identifiants des variables globales du shader
        GLint modelLoc = glGetUniformLocation(shaders.Program, "model");
        // Projection matrix (generate a perspective projection matrix)
        glm::mat4 projection;
        projection = glm::perspective(45.0f, (GLfloat)WIDTH / (GLfloat)HEIGHT, 0.1f, 350.0f);
        // Camera matrix
        glm::mat4 view;
        view = camera.GetViewMatrix();

        glm::mat4 model;
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));	
        glUniformMatrix4fv(glGetUniformLocation(shaders.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));


        // Activate texture 0
        glActiveTexture(GL_TEXTURE0);
        // Bind the texture
        glBindTexture(GL_TEXTURE_2D, texture);
        // Associate the texture with the shader
        glUniform1i(glGetUniformLocation(shaders.Program, "modelTexture"), 0);
        // Bind the VAO as a current object in the context of OpenGL
        glBindVertexArray(VAO);

        

        // Position and color of the sun light
        GLint lightPos = glGetUniformLocation(shaders.Program, "lightPos");
        GLint lightColor = glGetUniformLocation(shaders.Program, "lightColor");
        GLint ambientStrength = glGetUniformLocation(shaders.Program, "ambientStrength");
        GLint specularStrength = glGetUniformLocation(shaders.Program, "specularStrength");
        GLfloat ambStr(0.05f);
        GLfloat specStr(0.3f);

        // The sun light is placed relatively to the camera, so it follows the "user"
        glm::vec4 lPos(camera.Position.x, camera.Position.y + 300.0f, camera.Position.z, 1.0f);
        glm::vec3 lColor(1.0f, 1.0f, 1.0f);

        
        // Rotation matrice of the sun aroung the Z axe
        glm::mat4 rot;
        GLfloat angle = glm::mod(2*M_PI*(glfwGetTime()/TIME_CYCLE), 2*M_PI);
        rot = glm::rotate(rot, angle, glm::vec3(0.0f, 0.0f, 1.0f));
        lPos = rot * lPos;

        // Color of the sky in function of the time of the cycle
        glm::vec3 skyColor;
        // The sun is under the horizon line
        if (angle > TWILIGHT_ANGLE && angle < DAWN_ANGLE)
        {
            // The light is weak and with a deep blue color
            lColor = glm::vec3(0.0f, 0.0f, 0.5f);
            ambStr = 0.05f;

            // The sky is deep blue too
            skyColor = glm::vec3(0.0f, 0.0f, 0.1f);
        }
        // At the dawn
        else if (angle > DAWN_ANGLE && angle < 5*M_PI/3) 
        {
            // Variable for the variation of the color: 0 at the begining of the dawn, and 1 at the end
            double colorVariation = (DAWN_ANGLE - angle)/(DAWN_ANGLE - 5*M_PI/3);

            // Pinky-blue light,  +0.05f allows a continuity with the night.
            lColor = glm::vec3(pow(colorVariation,4) * 1.0f, pow(colorVariation, 2) * 1.0f, colorVariation * 1.0f + 0.05f);

            // Sky color
            // +0.1f allows a continuity with the night.
            skyColor = glm::vec3((-2*pow(colorVariation,2) + 2*colorVariation) * 1.0f, pow(colorVariation, 2) * 0.5f, pow(colorVariation, 4) * 1.0f + 0.1f);

            // Ambiant light continuous
            // Coherent with the values for day and night
            ambStr = colorVariation * 0.65f + 0.05f;
        }
        // At the twilight
        else if (angle > M_PI/3 && angle < TWILIGHT_ANGLE) 
        {
            // Variable for the variation of the color: 1 at the begining of the twilight, and 0 at the end
            double colorVariation = (TWILIGHT_ANGLE - angle)/(TWILIGHT_ANGLE - M_PI/4);

            // Orange light, +0.05f allows a continuity with the night.
            lColor = glm::vec3(colorVariation * 1.0f, pow(colorVariation, 2) * 1.0f, pow(colorVariation, 4) * 1.0f + 0.05f);

            // Sky color
            // +0.1f allows a continuity with the night.
            skyColor = glm::vec3((-2*pow(colorVariation,2) + 2*colorVariation) * 1.0f, pow(colorVariation, 2) * 0.5f, pow(colorVariation, 4) * 1.0f + 0.1f);

            // Ambiant light continuous
            // Coherent with the values for day and night
            ambStr = colorVariation * 0.65f + 0.05f;
        }
        // Daylight
        else {
            // The light is white and strong
            lColor = glm::vec3(1.0f, 1.0f, 1.0f);
            // String ambient light
            ambStr = 0.7f;

            // The sky is blue
            skyColor = glm::vec3(0.0f, 0.5f, 1.0f);
        }
        
        // Coloring the sky
        glClearColor(skyColor.x, skyColor.y, skyColor.z, 1.0f);
        // Update of the global variables for light
        glUniform3f(lightPos, lPos.x, lPos.y, lPos.z);
        glUniform3f(lightColor, lColor.x, lColor.y , lColor.z);
        glUniform1f(ambientStrength, ambStr);
        glUniform1f(specularStrength, specStr);

        
        // Recover the identifiers of the global variables of the shader
        GLint projLoc  = glGetUniformLocation(shaders.Program, "projection");
        // Update the global variables of the shader
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        // Recover the identifiers of the global variables of the shader
        GLint viewLoc  = glGetUniformLocation(shaders.Program, "view");
        // Update the global variables of the shader
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

        // Recover the identifiers of the global variables of the shader
        modelLoc = glGetUniformLocation(shaders.Program, "model");
        // Update the global variables of the shaders
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));


        // Street Lamp lights
        GLint lampPos = glGetUniformLocation(shaders.Program, "lampPos");
        GLint lampColor = glGetUniformLocation(shaders.Program, "lampColor");
        glm::vec3 lmpColor;

        // Between dawn and twilight, the street lamp light has a influence with a sodium lamp color
        if (angle > 2*M_PI/5 && angle < 8*M_PI/5)
        {
            lmpColor = glm::vec3(1.0f, 0.56f, 0.17f);
        }
        // During Daylight, StreetLamp off
        else {
            lmpColor = glm::vec3(0.0f, 0.0f, 0.0f);
        }

        // Update of the shaders variables
        glUniform3f(lampColor, lmpColor.x, lmpColor.y , lmpColor.z);

        // Position of the Street Lamp model
        glm::vec3 lmpPos(0.0f, 1.6f, 0.0f);
        glUniform3f(lampPos, lmpPos.x, lmpPos.y , lmpPos.z);



        
        // ------------ DRAWING -------------------

        // --- Draw the ground:
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);


        // --- Draw the objects:



        // Sun -------------------------------
        model = glm::mat4(1.0f);
        // Its position is relative to the camera in order to follow the "user"
        model = glm::translate(model, glm::vec3(camera.Position.x, camera.Position.y + 340.0f, camera.Position.z));
        model = rot*model;
        model = glm::scale(model, glm::vec3(13.0f));
        // Update shaders variables with a ambiant light and a color corresponding to the sun
        // And without the effect of the Street Lamp
        glUniform1f(ambientStrength, 5.0f);
        glUniform3f(lampColor, 0.0f, 0.0f , 0.0f);
        glUniform3f(lightColor, lColor.x + 0.4f, skyColor.y, 0.0f);
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

        // Draw the sun
        oSun.Draw(shaders);

        // Reinitialisation of the global variables
        glUniform3f(lampColor, lmpColor.x, lmpColor.y , lmpColor.z);
        glUniform1f(ambientStrength, ambStr);
        glUniform3f(lightColor, lColor.x, lColor.y , lColor.z);



        // Street Lamp ---------------
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(lmpPos.x, 0.0f, lmpPos.z));
        model = glm::scale(model, glm::vec3(0.3f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

        // Draw the object
        // When the Street Lamp is turned on 
        if(lmpColor.x != 0.0f)
        {   
            // The Street Lamp shine and is colored
            glUniform1f(ambientStrength, 0.7f);
            glUniform3f(lightColor, lmpColor.x, lmpColor.y, lmpColor.z);

            oSLampOn.Draw(shaders);

            // Reinitialisation of the global variables
            glUniform1f(ambientStrength, ambStr);
            glUniform3f(lightColor, lColor.x, lColor.y , lColor.z);

        
        } 
        // When the Street Lamp is turned off
        else {
            oSLampOff.Draw(shaders);
        }


        // Draw the model for house
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-3.0f, 0.0f, -5.0f));
        model = glm::scale(model, glm::vec3(0.05f));
        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        oHouse.Draw(shaders);

        // Draw the model for house2
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(2.0f, 0.0f, -5.0f));
        model = glm::scale(model, glm::vec3(0.05f));
        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        oHouse.Draw(shaders);


        // Draw the model for Tree
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-3.0f, 0.0f, -10.0f));
        model = glm::scale(model, glm::vec3(0.5f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        oTree.Draw(shaders);

        // Draw the model for Tree
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(3.0f, 0.0f, -10.0f));
        model = glm::scale(model, glm::vec3(0.5f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        oTree.Draw(shaders);

        // Draw the model for Tree
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-5.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.5f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        oTree.Draw(shaders);


        // Draw the model for swing
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(3.0f, 0.0f, -1.0f));
        model = glm::scale(model, glm::vec3(0.005f));
        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3( 0.0f, 0.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        oSwing.Draw(shaders);



        // Reinit
        // Model matrix (translation, rotation and scale)
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
        //model = glm::scale(model, glm::vec3(0.2f));
        // Update the global variables of the shader
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));



        // VBA is detached from the current object in the OpenGL context
        glBindVertexArray(0);

        // Exchange rendering buffers to update the window with new content
        glfwSwapBuffers(window);
        
    }
    

    // Delete the objects and buffer that we created earlier
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
    
    // End of the program, we clean up the context created by the GLFW
    glfwTerminate();
    return 0;
}
